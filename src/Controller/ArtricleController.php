<?php
/**
 * Created by PhpStorm.
 * User: sbeke
 * Date: 1/10/18
 * Time: 7:41 PM
 */

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class ArtricleController
{

    /**
     * @return Response
     * @Route("/")
     */
    function home()
    {
        return new Response('asd');
    }


    /**
     * @return Response
     * @Route("/news/{slug}")
     */
    function show($slug)
    {
        return new Response('hello ' . $slug);
    }

}